swap() {
    local -n arr=$1;  # Référence au tableau passé en argument
    local -n sel=$2;
    local index=$3;   # Index de l'élément à remonter
    local increment=$4;

    if (( index >= 0 )); then
        # Permuter avec l'élément précédent
        local temp="${arr[$index]}";
        arr[$index]="${arr[$index+$increment]}";
        arr[$index+$increment]="$temp";
    fi
}

selectmenu() {
	local POS=0;
	local RET=0;
	local MAX_LEN=0;
	local MULTI=0;
	local ALIGN=-1;
	local CYCLING=0;
	for var in "$@"; do
		if [[ "$var" =~  ^-s([0-9]+)$ ]]; then POS=$[${BASH_REMATCH[1]}-1]; shift; fi
		if [[ "$var" =~  ^-ro$ ]]; then RET=1; shift; fi
		if [[ "$var" =~  ^-a([0-9]+)$ ]]; then ALIGN=$[${BASH_REMATCH[1]}]; shift; fi
		if [[ "$var" =~  ^-m$ ]]; then MULTI=1; shift; fi
		if [[ "$var" =~  ^-cy$ ]]; then CYCLING=1; shift; fi
		if [[ "$var" =~  ^[^-].*$ ]]; then [[ ${#var} -ge $MAX_LEN ]] && MAX_LEN=${#var}; fi
	done
	local STYLE_NORMAL="\x1b[0m";
	local STYLE_HIGHLIGHT="\e[7m";
	local STYLE_SELECTED="\e[100m";
	local STYLE_OVERSELECTED="\e[41m";
	local SELECT=();
	local NB_ITEMS=$#;
	local IFS=;
	local i;
	local args=($@);
	local TEMP;
	[[ $ALIGN -lt 0 ]] && ALIGN=$MAX_LEN;
	[ $NB_ITEMS -eq 0 ] && return 1;

	while true;
	do
		i=0;
		for item in ${args[@]};
		do
			local STYLE="$STYLE_NORMAL";

			[[ "${SELECT[$i]}" != "" && $i -ne $POS ]] && STYLE="$STYLE_SELECTED";
			if [ $i -eq $POS ]; then [ "${SELECT[$i]}" != "" ] && STYLE="$STYLE_OVERSELECTED" || STYLE="$STYLE_HIGHLIGHT"; fi
			((i++));
			printf "$STYLE%-${ALIGN}s$STYLE_NORMAL\n" "$item";
		done
		read -rsn1 key;
		case "$key" in
			$'\x1b') # Arrow except escape
				read -rsn2 -t0.1 key;
				TEMP="${args[$POS]}";
				[ "$key" = $'[A' ] && ((POS--));
				[ "$key" = $'[B' ] && ((POS++));
				[ "$key" = $'[5' ] && args[$POS]="${args[$POS-1]}" && args[$((--POS%NB_ITEMS))]="$TEMP";
				[ "$key" = $'[6' ] && args[$POS]="${args[$(((POS+1) % NB_ITEMS))]}" && args[$((++POS%NB_ITEMS))]="$TEMP";
				[ "$key" = "" ] && return 1;
				;;
			" ") # Space
				if [ $POS -ge 0 ] && [ $MULTI -ne 0 ];
				then
					[[ "${SELECT[$POS]}" != "" ]] && SELECT[$POS]="" || SELECT[$POS]=$([ $RET -eq 1 ] && echo "$POS" || echo "${args[$POS]}");
					((POS++));
				fi
				;;
			"") # Enter
				if [ $POS -ge 0 ] && [ $MULTI -eq 0 ]; then ((POS++)); break; fi
				if [ $MULTI -ne 0 ] && [ ${#SELECT[@]} -gt 0 ]; then break; fi
				;;
		esac
		[ "$POS" -lt 0 ] && POS=$((CYCLING?NB_ITEMS-1:0));
		[ $POS -gt $[$NB_ITEMS-1] ] && POS=$((CYCLING?0:NB_ITEMS-1));

		echo -e "\033["$[$NB_ITEMS+1]"A";
	done

	if [ $MULTI -ne 0 ];
	then
		REPLY=(${SELECT[@]});
		#for i in ${SELECT[@]}; do echo $i; done
	else
		REPLY=$([[ $RET -eq 0 ]] && echo ${!POS} || echo $POS);
	fi
	return 0;
}
